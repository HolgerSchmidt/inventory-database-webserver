USE InventorySys;

/******************************************************************************/
/***                              Owner                                     ***/
/******************************************************************************/

INSERT INTO Owner (Name) VALUES ("Nightowl");

/******************************************************************************/
/***                              Inventory                                 ***/
/******************************************************************************/

INSERT INTO Inventory (Size, Owner) VALUES (100, 1);

/******************************************************************************/
/***                              Itemtype                                  ***/
/******************************************************************************/

INSERT INTO Itemtype (Title) VALUES ("Excalibur");

/******************************************************************************/
/***                              Item                                      ***/
/******************************************************************************/

INSERT INTO Item (Itemtype, Name, Inv) VALUES (2001, "The HOLY Sword", 1001);

/******************************************************************************/
/***                              Transaction                               ***/
/******************************************************************************/