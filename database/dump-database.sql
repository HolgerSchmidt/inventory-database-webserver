/******************************************************************************/
/***                              CREATE DATABASE                           ***/
/******************************************************************************/

DROP DATABASE IF EXISTS InventorySys;
CREATE DATABASE IF NOT EXISTS InventorySys;
USE InventorySys;


/******************************************************************************/
/***                              CREATE TABLE                              ***/
/******************************************************************************/

CREATE TABLE Owner (
	Owner_ID		INTEGER NOT NULL AUTO_INCREMENT,
	Name			VARCHAR(50),
	
	PRIMARY KEY (Owner_ID)
);

CREATE TABLE Inventory (
	Inv_ID			INTEGER NOT NULL AUTO_INCREMENT,
	Size			INTEGER,
	Owner			Integer,
	
	PRIMARY KEY (Inv_ID)
);

CREATE TABLE Item (
	Item_ID			INTEGER NOT NULL AUTO_INCREMENT,
	Itemtype		INTEGER,
	Inv				INTEGER,
	Name			VARCHAR(50),
	
	PRIMARY KEY (Item_ID)
);

CREATE TABLE Itemtype (
	Itemtype_ID		INTEGER NOT NULL AUTO_INCREMENT,
	Title			VARCHAR(50),
	
	PRIMARY KEY (Itemtype_ID)
);

CREATE TABLE Transaction(
	Transaction_ID	INTEGER NOT NULL AUTO_INCREMENT,
	Item			INTEGER,
	InvOld			INTEGER,	
	InvNew			INTEGER,
	
	PRIMARY KEY (Transaction_ID)
);


/******************************************************************************/
/***                              Primary Keys                              ***/
/******************************************************************************/

ALTER TABLE Owner AUTO_INCREMENT=1;
ALTER TABLE Inventory AUTO_INCREMENT=1001;
ALTER TABLE Itemtype AUTO_INCREMENT=2001;
ALTER TABLE Item AUTO_INCREMENT=3001;
ALTER TABLE Transaction AUTO_INCREMENT=20001;


/******************************************************************************/
/***                              Forgein Keys                              ***/
/******************************************************************************/

ALTER TABLE Inventory ADD FOREIGN KEY (Owner) REFERENCES Owner (Owner_ID);
ALTER TABLE Item ADD FOREIGN KEY (Itemtype) REFERENCES Itemtype (Itemtype_ID);
ALTER TABLE Item ADD FOREIGN KEY (Inv) REFERENCES Inventory (Inv_ID);
ALTER TABLE Transaction ADD FOREIGN KEY (Item) REFERENCES Item (Item_ID);
ALTER TABLE Transaction ADD FOREIGN KEY (InvOld) REFERENCES Inventory (Inv_ID);
ALTER TABLE Transaction ADD FOREIGN KEY (InvNew) REFERENCES Inventory (Inv_ID);