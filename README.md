# Inventory Database Webserver

Starten tut man die Container mit:
```
sudo docker-compose up
```

Die Scripte müssen noch ausführbar gemacht werden:
```
sudo chmod +x renewDatabase.sh
sudo chmod +x database/execSqlFiles.sh
```

Nun kann man mittles des Scripts die beiden Sql-Datein (Dump-Database + testdata) ausführen:
```
sudo ./renewDatabase
```
