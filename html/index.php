<DOCTYPE HTML>
<html>
	<head>
		<title> Inventory </title>
		<meta charset="utf-8">
  		<link rel="stylesheet" type="text/css" href="style.css">
	</head>
	<body>
		<div id='wrapper'>
			<div id='header'>
				<header>
					<h1> Inventory </h1>
				</header>
			</div>
			<div id="nav">
				<nav>
					<ul id="toplevelmenu">
						<li> <a href="index.php" aria-current="page"> Startseite </a> </li>
						<li> <a href="index.php"> Seite 2 </a> </li>
						<li> <a href="index.php"> Seite 3 </a> </li>
						<li> 
							<a> Seite 4  </a> 
							<ul id="secondlevelmenu">
								<li> <a href="index.php"> Seite 4a </a> </li>
								<li> <a href="index.php"> Seite 4b </a> </li>
							</ul>
						</li>
						<li> <a href="index.php"> Seite 5 </a> </li>
					</ul>
				</nav>
			</div>
			<div id='content'>
				<div class='querry'>
					<form action="" method="post">
						<textarea class='dbquerry' name="dbquery" cols="100" rows="5"></textarea><br>
						<input type="submit" value="submit" name="submit">
					</form>
				</div>
				<div class='table'>
					<?php
					// Verbindungsaufbau
					$host ="db";
					$user ="root";
					$password ="rootpass";
					$db ="InventorySys";
					
					$con = new mysqli($host, $user, $password, $db);
					if ($con->connect_error){
						echo "Jede Hilfe kommt zu spaet, Kevin ist tot". $con->connect_error;
					}
					
					$con->set_charset("utf8");
					
					
					if ($_POST["dbquery"] != ""){
						$dbquerry = trim(htmlspecialchars($_POST['dbquery']));
						if ($dbquerry[strlen($dbquerry)-1] == '\n'){
							$dbquerry = substr($dbquerry, 0, -1);
						}
						$res = $con->query($dbquerry);
						
						if($res && $res->num_rows>0){
							$values = $res->fetch_all(MYSQLI_ASSOC);
							$columns = array();

							$columns = array_keys($values[0]);

							echo "<table>";
							echo "<tr>";
								foreach($columns as $column) {
									echo "<th>" . $column . "</th>";
								}
							echo "</tr>";
							
							$even = False;
							foreach($values as $row) {
								if($even){
									echo "<tr class='rowEven'>";
									$even = False;
								}
								else {
									echo "<tr class='rowUneven'>";
									$even = True;
								}
								foreach($columns as $column) {
									echo "<td>" . $row[$column] . "</td>";
								}
								echo "</tr>";
							}
							echo "</table>";
						}
						else {
							echo "Ein Fehler ist aufgetreten.";
						}
					}
					?>
				</div>
			</div>				
			<div id='footer'>
				<a href="index.php"> Startseite </a> 
			</div>
		</div>
	</body>
</html>